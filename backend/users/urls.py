from django.urls import path
from .views import ListUserAPIView, RetrieveUserByIDAPIView, SearchUserByStringAPIView, \
    ListFollowersAPIView, RegisterUser, GetUserMeAPIView, UserValidationProfile, ToogleFollowAPIView, \
    ListFollowingAPIView

urlpatterns = [
    # REGISTRATION:
    # https://your-domain-name.com/backend/api/auth/registration/
    path('backend/api/auth/registration/', RegisterUser.as_view()),

    # https://your-domain-name.com/backend/api/auth/registration/validation/
    path('backend/api/auth/registration/validation/', UserValidationProfile.as_view()),

    # backend/api/users/
    path('backend/api/users/', ListUserAPIView.as_view()),

    # path('me/', RetrieveCurrentUserAPIView.as_view()),
    path('backend/api/users/<int:pk>', RetrieveUserByIDAPIView.as_view()),
    path('backend/api/users/me/', GetUserMeAPIView.as_view()),

    # path('?search', SearchUserByStringAPIView.as_view()),
    # backend/api/social/followers/
    path('backend/api/users/toggle-follow/<int:pk>/', ToogleFollowAPIView.as_view()),
    path('backend/api/users/followers/', ListFollowersAPIView.as_view()),
    path('backend/api/users/following/', ListFollowingAPIView.as_view()),
    path('backend/api/users/users/', ListUserAPIView.as_view()),
]
