import { useEffect } from "react"
import { useNavigate } from "react-router-dom"
import axios from 'axios';


// HOC
const wAuth = Page => {
  return (props) => {
    const navigate = useNavigate();
    const token = localStorage.getItem('token')
          
    useEffect(()=>{
      console.log('(WAuth comp) component did mount');

      // verify if token is valid
      const url = 'http://127.0.0.1:8000/backend/api/auth/token/verify';

      // fetching to verify if saved token is valid for an user
      axios.post(url, 
        { 
          token: localStorage.getItem('token') 
        })
        .then((response) => {
          console.log('(WAuth comp) response: ', response);

          if (response.status !== 200) {
            console.log('token is invalid, redirecting to log in page ...');
            navigate('/');
          }

        })
        .catch((error) => {
          console.log('error: ', error);

          navigate('/');
        })
      },[])

    return <Page {...props}></Page>
  }
}


export default wAuth