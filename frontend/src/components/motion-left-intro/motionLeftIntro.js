import React, { Component } from 'react';
import './motionLeftIntro.scss';
import twitterIcon from '../../assets/svgs/twitter_icon.svg'
import facebookIcon from '../../assets/svgs/facebook_icon.svg'
import instagramIcon from '../../assets/svgs/instagram_icon.svg'
import motionLogo from '../../assets/images/logo_white.png'
import appleButton from '../../assets/svgs/apple.svg'
import googleButton from '../../assets/svgs/google.svg'
 
const MotionLeftIntro = () => {
    return ( 
            <div className="left-container">
                <img className='motion_logo' src={motionLogo}></img>
                <h1 className='header-motion'>Motion</h1>
                <div className='text-motion'>
                    Connect with friends and the world<br />around you with Motion.
                </div >
                <div className="buttons-apple-google">
                <button className='apple-logo'>
                    <img src={appleButton}></img>
                </button>
                <button className='google-logo'>
                    <img src={googleButton}></img>
                </button>
                </div>
                <div className="icons-twitter-facebook-instagram">
                    <img className="icon-tfi-twitter" src={twitterIcon} ></img>
                    <img className="icon-tfi" src={facebookIcon} ></img>
                    <img className="icon-tfi" src={instagramIcon}></img>
                </div>
                <div className="copyright-motion">
                    © Motion 2018.All rights reserved.
                </div>
            </div>
     );
}
 
export default MotionLeftIntro;

