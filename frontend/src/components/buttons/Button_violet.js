import React from "react";
import "./Button_violet.scss";

const ButtonViolet = (props) => {
  return (
    <button
      className="Button_violet"
      onClick={props.handleClick}
    >
      {props.button_name}
    </button>
  );
}

export default ButtonViolet;
