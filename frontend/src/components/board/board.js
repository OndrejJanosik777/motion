import './board.scss';
import NewPost from '../newPost/newPost';
import Post from '../post/post';
import CommentShared from '../commentShared/commentShared';
import { useSelector, useDispatch } from 'react-redux';
import { useState } from 'react';
import AddPhotos from '../addPhotos/addPhotos';
// temporary photos for component
import photo1 from "../../assets/images/feedPics/image1.png";
import photo2 from "../../assets/images/feedPics/image2.png";
import photo3 from "../../assets/images/feedPics/image3.png";
import photo4 from "../../assets/images/feedPics/image4.png";


const Board = (props) => {
  // importing dispatch functions, selector, reduxState ...
  const dispatch = useDispatch();
  const selector = reduxState => reduxState;
  const reduxState = useSelector(selector);   // reduxState

  // temporary post objects for board - later fetching from backend
  let initialPosts = [
    {
      time: "Just Now",
      text: "Lorem ipsum dolor sit amet consectetur adipisicing elit. In veritatis ad autem dolore hic delectus soluta doloribus. Accusantium, commodi sapiente?",
      images: [photo1, photo2, photo3, photo4],
      nrOfLikes: 3,
    },
    {
      time: "Just Now",
      text: "Lorem ipsum dolor sit amet consectetur adipisicing elit. In veritatis ad autem dolore hic delectus soluta doloribus. Accusantium, commodi sapiente?",
      images: [photo1, photo2],
      nrOfLikes: 6,
    },
    {
      time: "1 Hour ago",
      text: "very nice picture",
      images: [photo3],
      nrOfLikes: 15,
    },
  ];

  // functions
  const saveNewPost = () => {
    console.log('saving new post...');
  }

  // component state:
  const [posts, setPosts] = useState(initialPosts);

  return (
    <div className='background'>
      <div className="board">
        <div className='left'>
          <NewPost toogleModal={props.toogleModal} saveNewPost={saveNewPost} />
          {posts.map((post) => {
            return <Post
              key={Math.random() * 100000}
              editable={true}
              post={post}
              loggedUser={reduxState.loggedUser}
            />;
          })}
        </div>
        <div className='right'>
          {/* <CommentShared /> */}
          {posts.map((post) => {
            return <Post
              key={Math.random() * 100000}
              editable={true}
              post={post}
              loggedUser={reduxState.loggedUser}
            />;
          })}
        </div>
        <div>
        </div>
      </div>
    </div>
  );
}

export default Board;