import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import reportWebVitals from "./reportWebVitals";
import { Provider } from "react-redux";
import { store } from "./store/store";
import { BrowserRouter, Routes, Route } from "react-router-dom";
// pages
import Verification from "./pages/verification/Verification";
import Congratulations from "./pages/congratulations/Congratulations";
import FindFriends from "./pages/findFriends/findFriends";
import Login from "./pages/Login/Login";
import Posts from "./pages/posts/posts";
import Profile from "./pages/profile/profile";
import SingUp from "./pages/sign-up/SignUp";
// components
import Board from "./components/board/board";
import CommentPosted from "./components/post/post";
import GreyNavbar from "./components/greyNavbar/greyNavbar";
import PostsNavBar from "./components/postsNavbar/postsNavbar";
import Cardboard from "./components/Cardboard/Cardboard.js";
import Mask from "./components/Mask/Mask";
import Card from "./components/Card/Card";
import SendedRequest from "./components/sendedRequest/sendedRequest";
import ReceivedRequest from "./components/receivedRequest/receivedRequest";
import SignUp from "./pages/sign-up/SignUp";
import UserInfo from "./components/userInfo/userInfo";
import WAuth from "./components/wauth/WAuth";


const AuthenticatedProfile = WAuth(Profile)
const AuthenticatedFindFriends = WAuth(FindFriends)
const AuthenticatedPosts = WAuth(Posts)
const AuthenticatedCommentPosted = WAuth(CommentPosted)
const AuthenticatedGreyNavbar = WAuth(GreyNavbar)
const AuthenticatedPostsNavBar = WAuth(PostsNavBar)
const AuthenticatedSendedRequest = WAuth(SendedRequest)
const AuthenticatedReceivedRequest = WAuth(ReceivedRequest)
const AuthenticatedMask = WAuth(Mask)
const AuthenticatedUserInfo = WAuth(UserInfo)
const AuthenticatedBoard = WAuth(Board)
const AuthenticatedCard = WAuth(Card)



ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <Routes>
        {/* login and authentication */}
        <Route path="/" element={<Login />} />
        <Route path="/signUp" element={<SignUp />} />
        <Route path="/congratulations" element={<Congratulations />} />
        <Route path="/verification" element={<Verification />} />
        {/* posts */}
        <Route path="/posts" element={<Posts />} />
        {/* <Route path="/find-friends" element={<AuthenticatedFindFriends />} /> */}
        <Route path="/find-friends" element={<AuthenticatedFindFriends />} />
        <Route path="/profile" element={<Profile />} />
        <Route path="/board" element={<Board />} />
        <Route path="/commentPosted" element={<CommentPosted />} />
        <Route path="/greyNavbar" element={<GreyNavbar />} />
        <Route path="/postnavbar" element={<PostsNavBar />} />
        <Route path="/Card" element={<Card />} />
        <Route path="/sended-request" element={<SendedRequest />} />
        <Route path="/received-request" element={<ReceivedRequest />} />
        <Route path="/Mask" element={<Mask />} />
        <Route path="/UserInfo" element={<UserInfo />} />
      </Routes>
    </BrowserRouter>
  </Provider>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
