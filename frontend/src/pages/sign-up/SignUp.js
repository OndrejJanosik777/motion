import { useState } from 'react';
import axios from 'axios';
import './SignUp.scss';
import { fetchAndDispatchCode } from '../../store/action'
import { useSelector, useDispatch } from 'react-redux'
import { useNavigate } from 'react-router-dom'
// import components
import MotionLeftIntro from '../../components/motion-left-intro/motionLeftIntro';
import Button_long from '../../components/buttons/Button_long';
import ButtonViolet from '../../components/buttons/Button_violet';
// import images, svg
import lock from '../../assets/svgs/password.svg'
import Mail from '../../assets/svgs/mail/mail';
// import lock from '../../assets/svgs/env.svg';
// import envelope from '../../assets/svgs/email.svg';
// import picture from '../../assets/svgs/env.svg';

const SignUp = () => {
  // hooks
  const selector = reduxState => reduxState.token;
  const dispatch = useDispatch();
  const navigate = useNavigate();

  // state
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  // some functions
  const onClickSignUp = () => {
    axios.get('https://ron-swanson-quotes.herokuapp.com/v2/quotes/1')
    .then(response => {
        console.log('response: ', response.data);
    })
  }

const onClick_SignIn = () => {
  navigate('/');
}
const onClick_Continue = () => {
  navigate('/congratulations');
}

  return (
      <div className='Sign-Up-page'>
        <div className='left-container'>
          <MotionLeftIntro />
        </div>
        <div className="right-container">
          <div className="sign-up-container">
            <div className="header_message">Already have an account?</div>
            <div className='sign-in-button'>
              <Button_long button_name={'SIGN IN'} handleClick={onClick_SignIn} />
            </div>
          </div>
          <div className='right-middle'>
              <div className="Sign-up-text">
                Sign Up
              </div>
              <div className='input-container'>
                <Mail height='16' width='20' fill='#A580FF' />
                <input
                  className='input-email'
                  type='email'
                  placeholder='email'
                  value={email}
                  onChange={e => setEmail(e.target.value)}>
                </input>
              </div>
          </div>
          <div className='sign-up-lower-part'>
            <div className='button-continue'>
              <ButtonViolet
                button_name={'CONTINUE'}
                handleClick={onClick_Continue}
              />
            </div>
            <div className='sign-up-progress'>
              <div className='progress-cirle-full'></div>
              <div className='progress-cirle-empty'></div>
              <div className='progress-cirle-empty'></div>
            </div>
          </div>
        </div>
      </div>
  )
}


export default SignUp
