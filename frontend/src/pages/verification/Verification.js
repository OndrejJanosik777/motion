import { useState } from 'react'
import './Verification.scss'
import { fetchAndDispatchVerification } from '../../store/action'
import { useSelector, useDispatch } from 'react-redux'
import { useNavigate, Link } from 'react-router-dom'
// import components
import MotionLeftIntro from '../../components/motion-left-intro/motionLeftIntro';
import ButtonViolet from '../../components/buttons/Button_violet';
// import assents

const Verifification = () => {
  // hooks
  const selector = reduxState => reduxState.token
  const dispatch = useDispatch()
  const navigate = useNavigate()

  // state
  const [email, setEmail] = useState('');
  const [code, setCode] = useState('');
  const [username, setUsername] = useState('');
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [password, setPassword] = useState('');
  const [passwordRepeat, setPasswordRepeat] = useState('');

  // functions
  const clickHandler = () => {
    dispatch((dispatch, getState) => fetchAndDispatchVerification(dispatch, getState,code, email, username, firstName, lastName, password, passwordRepeat, navigate))
  }

  const onClick_Complete = () => {

  }

  return (
      <div className='verification-page'>
        <div className="left-container">
          <MotionLeftIntro />
        </div>
        <div className="right-container">
          <div className="verification-text">
            Verification
          </div>
          <div className="verification-frame">
            <input 
              className='grid-1-3' 
              type='code' 
              placeholder='Validation code' 
              value={code} 
              onChange={e => setCode(e.target.value)}
            >
            </input>
            <input 
              className='grid-1' 
              type='email' 
              placeholder='email' 
              value={email} 
              onChange={e => setEmail(e.target.value)}>
            </input>
            <input 
              className='grid-1' 
              type='text' 
              placeholder='username' 
              value={username} 
              onChange={e => setUsername(e.target.value)}>
            </input>
            <input 
              className='grid-1' 
              type='text' 
              placeholder='First name' 
              value={firstName} 
              onChange={e => setFirstName(e.target.value)}>
            </input>
            <input 
              className='grid-1' 
              type='text' 
              placeholder='Last name' 
              value={lastName} 
              onChange={e => setLastName(e.target.value)}>
            </input>
            <input 
              className='grid-1' 
              type='password' 
              placeholder='Password' 
              value={password} 
              onChange={e => setPassword(e.target.value)}>
            </input>
            <input 
              className='grid-1' 
              type='password' 
              placeholder='Password repeat'
              value={passwordRepeat} 
              onChange={e => setPasswordRepeat(e.target.value)}>
            </input>
          </div>
          <div className='sign-up-lower-part'>
            <div className='button-continue'>
              <ButtonViolet
                button_name={'COMPLETE'}
                handleClick={onClick_Complete}
              />
            </div>
            <div className='sign-up-progress'>
              <div className='progress-cirle-empty'></div>
              <div className='progress-cirle-empty'></div>
              <div className='progress-cirle-full'></div>
            </div>
          </div>
        </div>
      </div>
  )
}


export default Verifification;
