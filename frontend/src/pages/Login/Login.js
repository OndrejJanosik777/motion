import { useState } from 'react';
import axios from 'axios';
import './Login.scss';
import { fetchAndDispatchToken } from '../../store/action'
import { fetchLoggedInUserData } from '../../store/action'
import { useSelector, useDispatch } from 'react-redux'
import { useNavigate } from 'react-router-dom'
// import components
import MotionLeftIntro from '../../components/motion-left-intro/motionLeftIntro';
import Button_long from '../../components/buttons/Button_long';
import ButtonViolet from '../../components/buttons/Button_violet';
// import images, svg
import avatar from '../../assets/svgs/avatar.svg'
import lock from '../../assets/svgs/password.svg'
import Button_short from '../../components/buttons/Button_short';
import Button_white_extendable from '../../components/buttons/Button_white_extendable';

const Login = () => {
  // hooks
  const selector = reduxState => reduxState.token;
  const dispatch = useDispatch();
  const navigate = useNavigate();

  // state
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  // const token = localStorage.getItem('token');

  // if (token) {
  //   console.log('Logged in ...');
  // }

  // functions
  const onClick_SignIn = () => {

    axios.post('http://127.0.0.1:8000/backend/api/auth/token/', {
      email: email,
      password: password,
    })
      .then(function (response) {
        // console.log('response: ', response.status);
        if (response.status === 200) {
          localStorage.setItem('token', response.data.access);
          console.log('(page Login) token stored to localStorage...', response.data.access);
          navigate('/posts');
        }
      })
      .catch(function (error) {
        console.log('(page Login) error: ', error);

        if (error.response.status === 401) {
          alert('Login failed: wrong username or password!');
        }

      });
  }

  const onClick_SignUp = () => {
    navigate('/signup');
  }

  return (
    <div className='Login-page'>
      <div className='left-container'>
        <MotionLeftIntro />
      </div>
      <div className="right-container">
        <div className="sign-up-container">
          <div className="header_message">Don't have an account ?</div>
          <div className='sign-in-button'>
            <Button_long button_name={'SIGN UP'} handleClick={onClick_SignUp} />
          </div>
        </div>
        <div className='right-middle'>
          <div className="Sign-in-text">
            Sign In
          </div>
          <div className='input-container'>
            <img className='avatar' src={avatar}></img>
            <input
              className='input-email'
              type='email'
              placeholder='email'
              value={email}
              onChange={e => setEmail(e.target.value)}>
            </input>
          </div>
          <div className='input-container'>
            <img className='password-logo' src={lock}></img>
            <input
              className='password-input'
              type='password'
              placeholder='password'
              value={password}
              onChange={e => setPassword(e.target.value)}>
            </input>
          </div>
        </div>
        <div className='button-sign-in'>
          <ButtonViolet
            button_name={'SIGN IN'}
            handleClick={onClick_SignIn}
          />
        </div>
      </div>
    </div>
  )
}


export default Login
