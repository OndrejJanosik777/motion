import './Congratulations.scss';
import { useNavigate } from 'react-router-dom';
import { useState } from 'react';
// import components
import MotionLeftIntro from '../../components/motion-left-intro/motionLeftIntro';
import ButtonViolet from '../../components/buttons/Button_violet';
// import assets
import tick from '../../assets/svgs/checkmark.svg';

const Congratulations = () => {
  // hooks
  const navigate = useNavigate();

  // state
  const [email, setEmail] = useState('dummy@dummy.com');

  const clickHandler = () => {
    navigate('/authentication');
  }

  const onClick_Continue = () => {
    navigate('/verification');
  }

  return (
      <div className='Congratulations-page'>
        <div className="left-container">
          <MotionLeftIntro />
        </div>
        <div className="right-container">
          <div className='right-middle'>
              <div className="Sign-up-text">
                Congratulations!
              </div>
              <img src={tick} ></img>
              <div className='message'>
                We've sent a confirmation code to your email !
              </div>
              <div className='message'>
                {email}
              </div>
          </div>
          <div className='sign-up-lower-part'>
            <div className='button-continue'>
              <ButtonViolet
                button_name={'CONTINUE'}
                handleClick={onClick_Continue}
              />
            </div>
            <div className='sign-up-progress'>
              <div className='progress-cirle-empty'></div>
              <div className='progress-cirle-full'></div>
              <div className='progress-cirle-empty'></div>
            </div>
          </div>
        </div>
      </div>
  )
}


export default Congratulations;